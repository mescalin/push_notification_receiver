package logger

import (
    "fmt"
    "log"
    "os"
    "runtime"
    "strings"
)

const (
    LOG_LEVEL_ERROR = iota
    LOG_LEVEL_WARNING
    LOG_LEVEL_INFO
    LOG_LEVEL_DEBUG
)

var (
    l = struct {
        logfile *os.File
        log *log.Logger
        loglevel int
    }{
        nil,
        nil,
        LOG_LEVEL_DEBUG,
    }
)

func LogClose() {
    if l.logfile != nil {
        l.logfile.Close()
        l.logfile = nil
    }
    if l.log != nil {
        l.log = nil
    }
}

func LogReload(path, level string) error {
    LogClose()

    f, e := os.OpenFile(path, os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
    if e != nil {
        return e
    }
    l.logfile = f
    l.log = log.New(l.logfile, "", log.LstdFlags)
    switch level {
    case "error":
        l.loglevel = LOG_LEVEL_ERROR
    case "warning":
        l.loglevel = LOG_LEVEL_WARNING
    case "info":
        l.loglevel = LOG_LEVEL_INFO
    default:
        l.loglevel = LOG_LEVEL_DEBUG
    }
    return nil
}

func Log(log_level int, v ...interface{}) {
    if l.loglevel < log_level {
        return
    }
    s := "!@#$"
    _, file, line, _ := runtime.Caller(1)
    fileline := fmt.Sprintf("%s:%d", file[strings.LastIndex(file, "/") + 1 : ], line)
    v[0] = fmt.Sprintf("%c | %-20s | %v", s[log_level], fileline, v[0])
    if l.logfile == nil {
        fmt.Println(v)
        return
    }
    l.log.Println(v...)
}
